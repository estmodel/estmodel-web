/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.web;

import ee.envir.estmodel.persistence.EstimateRepository;
import ee.envir.estmodel.persistence.entity.base.Estimate;
import ee.envir.estmodel.persistence.entity.base.Estimate.Parameter;
import ee.envir.estmodel.persistence.entity.base.Estimate.TimeStep;
import io.helidon.security.annotations.Authenticated;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Singleton
@Path("stations/{station-code}/estimates")
public class StationEstimatesResource {

    private final EstimateRepository repository;

    @Inject
    public StationEstimatesResource(EstimateRepository repository) {

        this.repository = repository;

    }

    @Authenticated
    @DELETE
    public void createStationEstimates(
            @PathParam("station-code") String stationCode,
            @QueryParam("parameter") @NotNull Parameter parameter,
            @QueryParam("start-year") @NotNull @Positive int startYear,
            @QueryParam("end-year") @NotNull @Positive int endYear) {

        this.repository.replaceByStation(stationCode,
                parameter, startYear, endYear);

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Estimate> getStationEstimates(
            @PathParam("station-code") String stationCode,
            @QueryParam("parameter") @DefaultValue("Q") Parameter parameter,
            @QueryParam("start-year") int startYear,
            @QueryParam("end-year") int endYear,
            @QueryParam("time-step") @DefaultValue("P1Y") TimeStep step) {

        return this.repository.findByStation(stationCode,
                parameter, startYear, endYear, step);

    }

}
