/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.web;

import ee.envir.estmodel.EstModel;
import jakarta.inject.Singleton;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Singleton
@Path("")
public class EstModelResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getIndexPage() {

        return """
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="utf-8">
                <meta name="robots" content="noindex">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="preconnect" href="https://fonts.googleapis.com">
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
                <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto&display=swap">
                <style>
                    a {
                        color: #000000;
                        text-decoration: none;
                    }
                    abbr {
                        color: #0072ce;
                        text-decoration: none;
                    }
                    body, html {
                        font: 400 14px/20px Roboto,Helvetica Neue,sans-serif;
                        display: flex;
                        letter-spacing: normal;
                        height: 100%;
                        margin: 0 auto;
                    }
                    h1 {
                        font: 400 56px/56px Roboto,Helvetica Neue,sans-serif;
                        letter-spacing: -.02em;
                        margin: 0 0 16px;
                    }
                    main {
                        margin: auto;
                        text-align: center;
                    }
                    main > a:hover {
                        text-decoration: underline;
                    }
                    @media (prefers-color-scheme: dark) {
                        a, abbr, body {
                            background-color: #121212;
                            color: #ffffff;
                        }
                    }
                    @media print {
                        main > a {
                            display: none;
                        }
                    }
                </style>
                <title>EstModel API</title>
            </head>
            <body>
                <main>
                    <h1><a href="https://estmodel.app" target="_blank"><abbr>Est</abbr>Model</a></h1>
                    <a rel="noopener" href="https://app.swaggerhub.com/apis-docs/estmodel/estmodel-api" target="_blank">Explore EstModel API documentation</a>
                </main>
            </body>
        </html>
        """;

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public EstModel runEstModel(@Valid @NotNull EstModel model) {

        model.run();
        return model;

    }

}
