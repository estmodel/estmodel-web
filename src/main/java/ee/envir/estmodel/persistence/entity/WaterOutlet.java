/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.persistence.entity.base.Estimate;
import ee.envir.estmodel.persistence.entity.base.Location;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Basic;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import static jakarta.persistence.EnumType.STRING;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.Noncacheable;

@Entity
@Table(name = "water_outlet")
@NamedQuery(name = "WaterOutlet.findAll", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.WaterOutlet(o.code, o.name, o.distance, o.length, o.type, o.waterType)
    FROM WaterOutlet o LEFT JOIN o.tags t
    WHERE (:code IS NULL OR LOCATE(UPPER(:code), o.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(o.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY o HAVING :count <= COUNT(o)
    ORDER BY o.name, o.code
    """)
@NamedQuery(name = "WaterOutlet.findByCountry", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.WaterOutlet(o.code, o.name, o.distance, o.length, o.type, o.waterType)
    FROM WaterOutlet o LEFT JOIN o.tags t JOIN o.district sd JOIN sd.district d
    WHERE :country = d.country.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), o.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(o.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY o HAVING :count <= COUNT(o)
    ORDER BY o.name, o.code
    """)
@NamedQuery(name = "WaterOutlet.findByDistrict", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.WaterOutlet(o.code, o.name, o.distance, o.length, o.type, o.waterType)
    FROM WaterOutlet o LEFT JOIN o.tags t JOIN o.district sd
    WHERE :district = sd.district.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), o.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(o.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY o HAVING :count <= COUNT(o)
    ORDER BY o.name, o.code
    """)
@NamedQuery(name = "WaterOutlet.findBySubdistrict", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.WaterOutlet(o.code, o.name, o.distance, o.length, o.type, o.waterType)
    FROM WaterOutlet o LEFT JOIN o.tags t
    WHERE :subdistrict = o.district.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), o.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(o.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY o HAVING :count <= COUNT(o)
    ORDER BY o.name, o.code
    """)
@NamedQuery(name = "WaterOutlet.findByRiver", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.WaterOutlet(o.code, o.name, o.distance, o.length, o.type, o.waterType)
    FROM WaterOutlet o LEFT JOIN o.tags t
    WHERE :river = o.river.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), o.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(o.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY o HAVING :count <= COUNT(o)
    ORDER BY o.distance, o.name, o.code
    """)
@JsonbPropertyOrder({"distance", "length", "type", "waterType", "district", "river"})
public class WaterOutlet extends Location {

    @Basic(optional = false)
    @Column(name = "distance", nullable = false)
    private double distance;

    @Basic(optional = false)
    @Column(name = "\"length\"", nullable = false)
    private double length;

    @NotNull
    @Enumerated(STRING)
    @Column(name = "type", length = 15)
    private Type type;

    @NotNull
    @Enumerated(STRING)
    @Column(name = "water_type", length = 15)
    private WaterType waterType;

    @ManyToOne(optional = false)
    @JoinColumn(name = "subdistrict", nullable = false)
    private Subdistrict district;

    @ManyToOne
    @JoinColumn(name = "river")
    private River river;

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true, targetEntity = WaterOutletEstimate.class)
    @JoinColumn(name = "water_outlet", nullable = false)
    @CascadeOnDelete
    private List<Estimate> estimates = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "water_outlet", nullable = false)
    @CascadeOnDelete
    @OrderBy("parameter, startDate")
    private List<WaterOutletMeasurement> measurements = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @ElementCollection
    @CollectionTable(name = "water_outlet_tag", joinColumns = @JoinColumn(name = "water_outlet"))
    @Column(name = "name", nullable = false)
    @CascadeOnDelete
    @OrderBy("name")
    private List<String> tags = new ArrayList<>();

    protected WaterOutlet() {
        super();
    }

    @JsonbCreator
    public WaterOutlet(String code) {
        super(code);
    }

    public WaterOutlet(String code, String name, double distance, double length, Type type, WaterType waterType) {
        super(code, name);
        this.distance = distance;
        this.length = length;
        this.type = type;
        this.waterType = waterType;
    }

    @PositiveOrZero
    public double getDistance() {
        return this.distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @PositiveOrZero
    public double getLength() {
        return this.length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public WaterType getWaterType() {
        return this.waterType;
    }

    public void setWaterType(WaterType waterType) {
        this.waterType = waterType;
    }

    @JsonbProperty("subdistrict")
    public Subdistrict getDistrict() {
        return this.district;
    }

    @JsonbTransient
    public void setDistrict(Subdistrict district) {

        district.getWaterOutlets().add(this);

        if (this.district != null) {
            this.district.getWaterOutlets().remove(this);
        }

        this.district = district;

    }

    public Optional<River> getRiver() {
        return Optional.ofNullable(this.river);
    }

    @JsonbTransient
    public void setRiver(River river) {

        if (river != null) {
            river.getWaterOutlets().add(this);
            if (river.getDistrict() != null) {
                this.setDistrict(river.getDistrict());
            }
        }

        if (this.river != null) {
            this.river.getWaterOutlets().remove(this);
        }

        this.river = river;

    }

    @Override
    public List<Estimate> getEstimates() {
        return this.estimates;
    }

    public void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }

    public List<WaterOutletMeasurement> getMeasurements() {
        return this.measurements;
    }

    protected void setMeasurements(List<WaterOutletMeasurement> measurements) {
        this.measurements = measurements;
    }

    @Override
    public List<String> getTags() {
        return this.tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public enum Type {

        AQUACULTURE, INDUSTRIAL, MUNICIPAL;

    }

    public enum WaterType {

        COOLANT_WATER,
        MINING_WATER,
        QUARRY_WATER,
        STORMWATER,
        TREATED_WATER,
        WASTEWATER;

    }

}
