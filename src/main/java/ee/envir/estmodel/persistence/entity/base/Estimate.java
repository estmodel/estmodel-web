/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity.base;

import ee.envir.estmodel.persistence.entity.AssessmentUnit;
import ee.envir.estmodel.persistence.entity.AssessmentUnitEstimate;
import ee.envir.estmodel.persistence.entity.WaterOutlet;
import ee.envir.estmodel.persistence.entity.WaterOutletEstimate;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import static jakarta.persistence.EnumType.STRING;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;
import org.eclipse.persistence.annotations.AdditionalCriteria;
import org.eclipse.persistence.annotations.Cache;
import static org.eclipse.persistence.config.CacheIsolationType.ISOLATED;

@MappedSuperclass
@AdditionalCriteria("""
    this.parameter IN :parameters
    AND this.startDate >= :startDate AND this.endDate <= :endDate
    """)
@Cache(isolation = ISOLATED, size = 366)
@JsonbPropertyOrder({"parameter", "startDate", "endDate", "value", "unit"})
public class Estimate implements Serializable {

    @JsonbTransient
    @Id
    @Column(name = "code", length = 15)
    protected String code;

    @Enumerated(STRING)
    @Id
    @Column(name = "\"parameter\"", length = 7)
    private Parameter parameter;

    @Id
    @Column(name = "start_date")
    private LocalDate startDate;

    @Basic(optional = false)
    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @Basic(optional = false)
    @Column(name = "\"value\"", nullable = false)
    private double value;

    public Estimate() {

    }

    public Estimate(Parameter parameter, LocalDate startDate, LocalDate endDate, double value) {

        this.parameter = parameter;
        this.startDate = startDate;
        this.endDate = endDate;
        this.value = value;

    }

    protected Estimate(String code, Parameter parameter, LocalDate date, double value) {

        this.code = code;
        this.parameter = parameter;
        this.startDate = date;
        this.endDate = date;
        this.value = value;

    }

    @Override
    public final boolean equals(Object object) {
        return object instanceof Estimate other
                && Objects.equals(this.code, other.code)
                && this.parameter == other.parameter
                && Objects.equals(this.startDate, other.startDate);
    }

    @NotNull
    public Parameter getParameter() {
        return this.parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    @NotNull
    public LocalDate getStartDate() {
        return this.startDate;
    }

    public void setStartDate(LocalDate date) {
        this.startDate = date;
    }

    @NotNull
    public LocalDate getEndDate() {
        return this.endDate;
    }

    public void setEndDate(LocalDate date) {
        this.endDate = date;
    }

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return this.parameter.unit;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(this.code, this.parameter, this.startDate);
    }

    public AssessmentUnitEstimate with(AssessmentUnit assessmentUnit) {
        var estimate = new AssessmentUnitEstimate();
        estimate.setAssessmentUnit(assessmentUnit);
        estimate.setParameter(this.getParameter());
        estimate.setStartDate(this.getStartDate());
        estimate.setEndDate(this.getEndDate());
        estimate.setValue(this.getValue());
        return estimate;
    }

    public WaterOutletEstimate with(WaterOutlet outlet) {
        var estimate = new WaterOutletEstimate();
        estimate.setWaterOutlet(outlet);
        estimate.setParameter(this.getParameter());
        estimate.setStartDate(this.getStartDate());
        estimate.setEndDate(this.getEndDate());
        estimate.setValue(this.getValue());
        return estimate;
    }

    public enum Parameter {

        AOX("t"),
        BOD5("t"),
        BOD7("t"),
        CD("t"),
        COD_CR("t"),
        COD_MN("t"),
        CR("t"),
        CU("t"),
        HG("t"),
        NH4_N("t"),
        NI("t"),
        NO2_N("t"),
        NO3_N("t"),
        OIL("t"),
        PB("t"),
        PO4_P("t"),
        Q("m³/s"),
        SS("t"),
        TN("t"),
        TOC("t"),
        TP("t"),
        V("m³"),
        ZN("t");

        final String unit;

        Parameter(String unit) {
            this.unit = unit;
        }

    }

    public enum TimeStep {

        P1D(Period.ofDays(1)),
        P1M(Period.ofMonths(1)),
        P3M(Period.ofMonths(3)),
        P1Y(Period.ofYears(1));

        private final Period period;

        TimeStep(Period period) {
            this.period = period;
        }

        public Period toPeriod() {
            return this.period;
        }

    }

}
