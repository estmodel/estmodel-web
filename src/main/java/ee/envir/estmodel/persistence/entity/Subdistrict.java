/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.model.RiverBasinDistrictSubunit;
import ee.envir.estmodel.persistence.entity.base.Catchment;
import ee.envir.estmodel.persistence.entity.base.Estimate;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.Noncacheable;

@Entity
@Table(name = "subdistrict")
@NamedQuery(name = "Subdistrict.findAll", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Subdistrict(sd.code, sd.name, sd.area)
    FROM Subdistrict sd LEFT JOIN sd.tags t
    WHERE (:code IS NULL OR LOCATE(UPPER(:code), sd.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(sd.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY sd HAVING :count <= COUNT(sd)
    ORDER BY sd.code
    """)
@NamedQuery(name = "Subdistrict.findByCountry", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Subdistrict(sd.code, sd.name, sd.area)
    FROM Subdistrict sd LEFT JOIN sd.tags t JOIN sd.district d
    WHERE :country = d.country.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), sd.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(sd.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY sd HAVING :count <= COUNT(sd)
    ORDER BY sd.code
    """)
@NamedQuery(name = "Subdistrict.findByDistrict", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Subdistrict(sd.code, sd.name, sd.area)
    FROM Subdistrict sd LEFT JOIN sd.tags t
    WHERE :district = sd.district.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), sd.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(sd.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY sd HAVING :count <= COUNT(sd)
    ORDER BY sd.code
    """)
public class Subdistrict extends Catchment<SubdistrictModel>
        implements RiverBasinDistrictSubunit<River, Station> {

    @ManyToOne(optional = false)
    @JoinColumn(name = "district", nullable = false)
    private District district;

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true, targetEntity = SubdistrictEstimate.class)
    @JoinColumn(name = "subdistrict", nullable = false)
    @CascadeOnDelete
    private List<Estimate> estimates = new ArrayList<>();

    @JsonbTransient
    @OneToMany(mappedBy = "district")
    @CascadeOnDelete
    private List<River> rivers = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @ElementCollection
    @CollectionTable(name = "subdistrict_tag", joinColumns = @JoinColumn(name = "subdistrict"))
    @Column(name = "name", nullable = false)
    @CascadeOnDelete
    @OrderBy("name")
    private List<String> tags = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @OneToMany(mappedBy = "district")
    @CascadeOnDelete
    private List<WaterOutlet> waterOutlets = new ArrayList<>();

    protected Subdistrict() {
        super();
    }

    @JsonbCreator
    public Subdistrict(String code) {
        super(code);
    }

    public Subdistrict(String code, String name, double area) {
        super(code, name, area);
    }

    @Override
    public double discharge(Object parameter, LocalDate startDate, LocalDate endDate) {

        var stations = this.findEnclosingStations(parameter, startDate, endDate).toList();

        if (stations.isEmpty()) {
            stations = this.getDistrict()
                    .findEnclosingStations(parameter, startDate, endDate).toList();
        }

        if (stations.isEmpty()) {
            stations = this.getDistrict().getCountry()
                    .findEnclosingStations(parameter, startDate, endDate).toList();
        }

        double monitoredArea = stations.stream()
                .mapToDouble(Station::getCalculationArea)
                .sum();

        double monitoredDischarge = stations.stream()
                .mapToDouble(s -> s.discharge(parameter, startDate, endDate))
                .sum();

        return monitoredDischarge / monitoredArea * this.getArea();

    }

    @Override
    public Stream<Station> findEnclosingStations(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.getRivers().stream()
                .filter(r -> r.isInternalWater()
                && r.isDistrictRiver()
                && r.getParentRiver()
                        .filter(pr -> this.equals(pr.getDistrict())
                        && pr.isInternalWater()
                        && pr.isDistrictRiver()).isEmpty())
                .flatMap(r -> r.findEnclosingStations(parameter, startDate, endDate));
    }

    public District getDistrict() {
        return this.district;
    }

    @JsonbTransient
    public void setDistrict(District district) {

        district.getSubdistricts().add(this);

        if (this.district != null) {
            this.district.getSubdistricts().remove(this);
        }

        this.district = district;

    }

    @Override
    public List<Estimate> getEstimates() {
        return this.estimates;
    }

    protected void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }

    @Override
    public List<River> getRivers() {
        return this.rivers;
    }

    protected void setRivers(List<River> rivers) {
        this.rivers = rivers;
    }

    @Override
    public List<String> getTags() {
        return this.tags;
    }

    protected void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<WaterOutlet> getWaterOutlets() {
        return this.waterOutlets;
    }

    protected void setWaterOutlets(List<WaterOutlet> outlets) {
        this.waterOutlets = outlets;
    }

    @Override
    public double specificDischarge(Object parameter, LocalDate startDate, LocalDate endDate) {

        return super.discharge(parameter, startDate, endDate) / this.getArea();

    }

}
