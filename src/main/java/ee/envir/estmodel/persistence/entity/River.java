/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.model.RiverBasin;
import ee.envir.estmodel.persistence.entity.base.Estimate;
import ee.envir.estmodel.persistence.entity.base.Subcatchment;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Basic;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.Noncacheable;

@Entity
@Table(name = "river")
@NamedQuery(name = "River.findAll", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.River(r.code, r.name, r.area, r.overlapArea, r.countryArea, r.calculationArea, r.distance, r.length)
    FROM River r LEFT JOIN r.tags t
    WHERE (:code IS NULL OR LOCATE(UPPER(:code), r.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(r.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY r HAVING :count <= COUNT(r)
    ORDER BY r.name, r.code
    """)
@NamedQuery(name = "River.findByCountry", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.River(r.code, r.name, r.area, r.overlapArea, r.countryArea, r.calculationArea, r.distance, r.length)
    FROM River r LEFT JOIN r.tags t JOIN r.district sd JOIN sd.district d
    WHERE :country = d.country.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), r.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(r.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY r HAVING :count <= COUNT(r)
    ORDER BY r.name, r.code
    """)
@NamedQuery(name = "River.findByDistrict", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.River(r.code, r.name, r.area, r.overlapArea, r.countryArea, r.calculationArea, r.distance, r.length)
    FROM River r LEFT JOIN r.tags t JOIN r.district sd
    WHERE :district = sd.district.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), r.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(r.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY r HAVING :count <= COUNT(r)
    ORDER BY r.name, r.code
    """)
@NamedQuery(name = "River.findBySubdistrict", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.River(r.code, r.name, r.area, r.overlapArea, r.countryArea, r.calculationArea, r.distance, r.length)
    FROM River r LEFT JOIN r.tags t
    WHERE :subdistrict = r.district.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), r.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(r.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY r HAVING :count <= COUNT(r)
    ORDER BY r.name, r.code
    """)
@NamedQuery(name = "River.findByRiver", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.River(r.code, r.name, r.area, r.overlapArea, r.countryArea, r.calculationArea, r.distance, r.length)
    FROM River r LEFT JOIN r.tags t
    WHERE :river = r.parentRiver.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), r.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(r.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY r HAVING :count <= COUNT(r)
    ORDER BY r.distance, r.name, r.code
    """)
@NamedQuery(name = "River.orphanBySubdistrict", query = """
    UPDATE River r SET r.parentRiver = NULL
    WHERE :subdistrict = r.parentRiver.district.code
    """)
@NamedQuery(name = "River.orphanByRiver", query = """
    UPDATE River r SET r.parentRiver = NULL
    WHERE :river = r.parentRiver.code
    """)
@JsonbPropertyOrder({"length", "district", "parentRiver"})
public class River extends Subcatchment<RiverModel>
        implements RiverBasin<River, Station> {

    @Basic(optional = false)
    @Column(name = "\"length\"", nullable = false)
    private double length;

    @ManyToOne(optional = false)
    @JoinColumn(name = "subdistrict", nullable = false)
    private Subdistrict district;

    @ManyToOne
    @JoinColumn(name = "parent_river")
    private River parentRiver;

    @JsonbTransient
    @OrderBy("distance")
    @OneToMany(mappedBy = "river", targetEntity = RiverAdjustment.class)
    @CascadeOnDelete
    private List<Adjustment> adjustments = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true, targetEntity = RiverEstimate.class)
    @JoinColumn(name = "river", nullable = false)
    @CascadeOnDelete
    private List<Estimate> estimates = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @OneToMany(mappedBy = "river")
    @CascadeOnDelete
    private List<Lake> lakes = new ArrayList<>();

    @JsonbTransient
    @OneToMany(mappedBy = "river")
    @CascadeOnDelete
    private List<Station> stations = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @ElementCollection
    @CollectionTable(name = "river_tag", joinColumns = @JoinColumn(name = "river"))
    @Column(name = "name", nullable = false)
    @CascadeOnDelete
    @OrderBy("name")
    private List<String> tags = new ArrayList<>();

    @JsonbTransient
    @OneToMany(mappedBy = "parentRiver")
    private List<River> tributaries = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @OneToMany(mappedBy = "river")
    @CascadeOnDelete
    private List<WaterOutlet> waterOutlets = new ArrayList<>();

    protected River() {
        super();
    }

    @JsonbCreator
    public River(String code) {
        super(code);
    }

    public River(String code, String name,
            double area, double overlapArea,
            double countryArea, double calculationArea,
            double distance, double length) {

        super(code, name, area, overlapArea, countryArea, calculationArea, distance);
        this.length = length;

    }

    @Override
    public double discharge(Object parameter, LocalDate startDate, LocalDate endDate) {
        return RiverBasin.super.discharge(parameter, startDate, endDate);
    }

    @Override
    public double getLength() {
        return this.length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    @JsonbProperty("subdistrict")
    public Subdistrict getDistrict() {
        return this.district;
    }

    @JsonbTransient
    public void setDistrict(Subdistrict district) {

        if (this.parentRiver != null
                && !this.parentRiver.getDistrict().getDistrict().equals(district.getDistrict())) {

            throw new IllegalArgumentException();
        }

        district.getRivers().add(this);

        if (this.district != null) {
            this.district.getRivers().remove(this);
        }

        this.district = district;

    }

    @Override
    public Optional<River> getParentRiver() {
        return Optional.ofNullable(this.parentRiver);
    }

    @JsonbTransient
    public void setParentRiver(River river) {

        var parent = river;

        while (parent != null) {
            if (this.equals(parent)) {
                throw new IllegalArgumentException();
            }
            parent = parent.parentRiver;
        }

        if (river != null) {
            river.getTributaries().add(this);
            if (river.getDistrict() != null && (this.getDistrict() == null
                    || !this.getDistrict().getDistrict().equals(river.getDistrict().getDistrict()))) {

                this.setDistrict(river.getDistrict());
            }
        }

        if (this.parentRiver != null) {
            this.parentRiver.getTributaries().remove(this);
        }

        this.parentRiver = river;

    }

    @Override
    public List<Adjustment> getAdjustments() {
        return this.adjustments;
    }

    public void setAdjustments(List<Adjustment> adjustments) {
        this.adjustments = adjustments;
    }

    @Override
    public List<Estimate> getEstimates() {
        return this.estimates;
    }

    protected void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }

    public List<Lake> getLakes() {
        return this.lakes;
    }

    protected void setLakes(List<Lake> lakes) {
        this.lakes = lakes;
    }

    @Override
    public List<Station> getStations() {
        return this.stations;
    }

    protected void setStations(List<Station> stations) {
        this.stations = stations;
    }

    @Override
    public List<String> getTags() {
        return this.tags;
    }

    protected void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public List<River> getTributaries() {
        return this.tributaries;
    }

    protected void setTributaries(List<River> rivers) {
        this.tributaries = rivers;
    }

    public List<WaterOutlet> getWaterOutlets() {
        return this.waterOutlets;
    }

    protected void setWaterOutlets(List<WaterOutlet> outlets) {
        this.waterOutlets = outlets;
    }

    @JsonbTransient
    public boolean isInternalWater() {
        // Country area must be over half of the total area
        return this.getCountryArea() / this.getArea() > 0.5;
    }

}
