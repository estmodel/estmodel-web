/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.persistence.entity.base.Estimate;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import java.time.LocalDate;
import org.eclipse.persistence.annotations.PrimaryKey;

@Entity
@Table(name = "country_estimate")
@PrimaryKey(columns = {
    @Column(name = "country"),
    @Column(name = "\"parameter\""),
    @Column(name = "start_date")
})
@NamedQuery(name = "Estimate.deleteByCountry", query = """
    DELETE FROM CountryEstimate e
    WHERE e.code = :country
    """)
@NamedQuery(name = "Estimate.findByCountryGroupByP1D", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Estimate(e.parameter, e.startDate, e.endDate, e.value)
    FROM CountryEstimate e
    WHERE e.code = :country
    ORDER BY e.parameter, e.startDate
    """)
@NamedQuery(name = "Estimate.findByCountryGroupByP1M", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Estimate(e.parameter, MIN(e.startDate), MAX(e.endDate),
    CASE e.parameter WHEN ee.envir.estmodel.persistence.entity.base.Estimate.Parameter.Q THEN AVG(e.value) ELSE SUM(e.value) END)
    FROM CountryEstimate e
    WHERE e.code = :country
    GROUP BY e.parameter, EXTRACT(YEAR FROM e.startDate), EXTRACT(MONTH FROM e.startDate)
    ORDER BY e.parameter, MIN(e.startDate)
    """)
@NamedQuery(name = "Estimate.findByCountryGroupByP3M", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Estimate(e.parameter, MIN(e.startDate), MAX(e.endDate),
    CASE e.parameter WHEN ee.envir.estmodel.persistence.entity.base.Estimate.Parameter.Q THEN AVG(e.value) ELSE SUM(e.value) END)
    FROM CountryEstimate e
    WHERE e.code = :country
    GROUP BY e.parameter, EXTRACT(YEAR FROM e.startDate), EXTRACT(QUARTER FROM e.startDate)
    ORDER BY e.parameter, MIN(e.startDate)
    """)
@NamedQuery(name = "Estimate.findByCountryGroupByP1Y", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.base.Estimate(e.parameter, MIN(e.startDate), MAX(e.endDate),
    CASE e.parameter WHEN ee.envir.estmodel.persistence.entity.base.Estimate.Parameter.Q THEN AVG(e.value) ELSE SUM(e.value) END)
    FROM CountryEstimate e
    WHERE e.code = :country
    GROUP BY e.parameter, EXTRACT(YEAR FROM e.startDate)
    ORDER BY e.parameter, MIN(e.startDate)
    """)
@AttributeOverride(name = "code", column = @Column(name = "country", length = 15))
public class CountryEstimate extends Estimate {

    public CountryEstimate() {
        super();
    }

    public CountryEstimate(Country country, Parameter parameter, LocalDate date, double value) {
        super(country.getCode(), parameter, date, value);
    }

    public void setCountry(Country country) {
        super.code = country.getCode();
    }

}
