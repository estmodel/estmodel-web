/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity.base;

import ee.envir.estmodel.persistence.entity.Station;
import ee.envir.estmodel.persistence.entity.StationMeasurement;
import ee.envir.estmodel.persistence.entity.WaterOutlet;
import ee.envir.estmodel.persistence.entity.WaterOutletMeasurement;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import static jakarta.persistence.EnumType.STRING;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.function.Predicate;
import org.eclipse.persistence.annotations.AdditionalCriteria;
import org.eclipse.persistence.annotations.Cache;
import static org.eclipse.persistence.config.CacheIsolationType.ISOLATED;

@MappedSuperclass
@AdditionalCriteria("""
    this.parameter IN :parameters
    AND this.startDate >= :startDate AND this.endDate <= :endDate
    """)
@Cache(isolation = ISOLATED, size = 366)
@JsonbPropertyOrder({"parameter", "startDate", "endDate",
    "value", "quantificationLimit", "unit", "qualityFlag", "uncertainty", "count"})
public class Measurement implements Serializable {

    @JsonbTransient
    @Id
    @Column(name = "code", length = 15)
    protected String code;

    @Enumerated(STRING)
    @Id
    @Column(name = "\"parameter\"", length = 7)
    private Parameter parameter;

    @JsonbTransient
    @Enumerated(STRING)
    @Id
    @Column(name = "type", length = 4)
    private Type type = Type.MEAN;

    @Id
    @Column(name = "start_date")
    private LocalDate startDate;

    @Basic(optional = false)
    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @Basic(optional = false)
    @Column(name = "\"value\"", nullable = false)
    private double value;

    @Column(name = "quantification_limit")
    private Double quantificationLimit;

    @Column(name = "uncertainty")
    private Integer uncertainty;

    @Basic(optional = false)
    @Column(name = "\"count\"", nullable = false)
    private long count = 1;

    @Enumerated(STRING)
    @Column(name = "quality_flag", length = 15)
    private QualityFlag qualityFlag;

    public Measurement() {

    }

    public Measurement(Parameter parameter, LocalDate startDate, LocalDate endDate,
            double value, long count) {

        this(parameter, startDate, endDate, value, null, null, count, null);

    }

    public Measurement(Parameter parameter, LocalDate startDate, LocalDate endDate,
            double value, long count, QualityFlag qualityFlag) {

        this(parameter, startDate, endDate, value, null, null, count, qualityFlag);

    }

    public Measurement(Parameter parameter, LocalDate startDate, LocalDate endDate,
            double value, Double quantificationLimit, Integer uncertainty, long count) {

        this(parameter, startDate, endDate, value,
                quantificationLimit, uncertainty, count, null);

    }

    public Measurement(Parameter parameter, LocalDate startDate, LocalDate endDate,
            double value, Double quantificationLimit, Integer uncertainty,
            long count, QualityFlag qualityFlag) {

        this.parameter = parameter;
        this.startDate = startDate;
        this.endDate = endDate;
        this.value = value;
        this.quantificationLimit = quantificationLimit;
        this.uncertainty = uncertainty;
        this.count = count;
        this.qualityFlag = qualityFlag;

    }

    @Override
    public final boolean equals(Object object) {
        return object instanceof Measurement other
                && Objects.equals(this.code, other.code)
                && this.parameter == other.parameter
                && this.type == other.type
                && Objects.equals(this.startDate, other.startDate);

    }

    @NotNull
    public Parameter getParameter() {
        return this.parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @NotNull
    public LocalDate getStartDate() {
        return this.startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @NotNull
    public LocalDate getEndDate() {
        return this.endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Double getQuantificationLimit() {
        return this.quantificationLimit;
    }

    public void setQuantificationLimit(Double limit) {
        this.quantificationLimit = limit;
    }

    public String getUnit() {
        return this.parameter.unit;
    }

    @Min(0)
    @Max(100)
    public Integer getUncertainty() {
        return this.uncertainty;
    }

    public void setUncertainty(Integer uncertainty) {
        this.uncertainty = uncertainty;
    }

    @Positive
    public long getCount() {
        return this.count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public QualityFlag getQualityFlag() {
        return this.qualityFlag;
    }

    public void setQualityFlag(QualityFlag flag) {
        this.qualityFlag = flag;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(this.code, this.parameter, this.type, this.startDate);
    }

    public StationMeasurement with(Station station) {
        var measurement = new StationMeasurement();
        measurement.setStation(station);
        measurement.setParameter(this.getParameter());
        measurement.setType(this.getType());
        measurement.setStartDate(this.getStartDate());
        measurement.setEndDate(this.getEndDate());
        measurement.setValue(this.getValue());
        measurement.setQuantificationLimit(this.getQuantificationLimit());
        measurement.setUncertainty(this.getUncertainty());
        measurement.setCount(measurement.getCount());
        measurement.setQualityFlag(this.getQualityFlag());
        return measurement;
    }

    public WaterOutletMeasurement with(WaterOutlet outlet) {
        var measurement = new WaterOutletMeasurement();
        measurement.setWaterOutlet(outlet);
        measurement.setParameter(this.getParameter());
        measurement.setType(this.getType());
        measurement.setStartDate(this.getStartDate());
        measurement.setEndDate(this.getEndDate());
        measurement.setValue(this.getValue());
        measurement.setQuantificationLimit(this.getQuantificationLimit());
        measurement.setUncertainty(this.getUncertainty());
        measurement.setCount(measurement.getCount());
        measurement.setQualityFlag(this.getQualityFlag());
        return measurement;
    }

    public enum Parameter {

        AOX("mg/l"),
        BOD5("mg/l"),
        BOD7("mg/l"),
        CD("mg/l"),
        COD_CR("mg/l"),
        COD_MN("mg/l"),
        CR("mg/l"),
        CU("mg/l"),
        H("m"),
        HG("mg/l"),
        NH4_N("mg/l"),
        NI("mg/l"),
        NO2_N("mg/l"),
        NO3_N("mg/l"),
        OIL("mg/l"),
        PB("mg/l"),
        PO4_P("mg/l"),
        Q("m³/s"),
        SS("mg/l"),
        T("°C"),
        TN("mg/l"),
        TOC("mg/l"),
        TP("mg/l"),
        ZN("mg/l");

        final String unit;

        Parameter(String unit) {
            this.unit = unit;
        }

    }

    public enum QualityFlag implements Predicate<Measurement> {

        BELOW_LOQ, OUTLIER, UNCHECKED;

        @Override
        public boolean test(Measurement measurement) {
            return this == measurement.getQualityFlag();
        }

    }

    public enum Type implements Predicate<Measurement> {

        MAXIMUM, MEAN, MINIMUM, VALIDATION;

        @Override
        public boolean test(Measurement measurement) {
            return this == measurement.getType();
        }

    }

}
