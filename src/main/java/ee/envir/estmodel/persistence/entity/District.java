/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.model.RiverBasinDistrict;
import ee.envir.estmodel.persistence.entity.base.Catchment;
import ee.envir.estmodel.persistence.entity.base.Estimate;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.Noncacheable;

@Entity
@Table(name = "district")
@NamedQuery(name = "District.findAll", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.District(d.code, d.name, d.area)
    FROM District d LEFT JOIN d.tags t
    WHERE (:code IS NULL OR LOCATE(UPPER(:code), d.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(d.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY d HAVING :count <= COUNT(d)
    ORDER BY d.code
    """)
@NamedQuery(name = "District.findByCountry", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.District(d.code, d.name, d.area)
    FROM District d LEFT JOIN d.tags t
    WHERE :country = d.country.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), d.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(d.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY d HAVING :count <= COUNT(d)
    ORDER BY d.code
    """)
public class District extends Catchment<DistrictModel>
        implements RiverBasinDistrict<Station> {

    @ManyToOne(optional = false)
    @JoinColumn(name = "country", nullable = false)
    private Country country;

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true, targetEntity = DistrictEstimate.class)
    @JoinColumn(name = "district", nullable = false)
    @CascadeOnDelete
    private List<Estimate> estimates = new ArrayList<>();

    @JsonbTransient
    @OneToMany(mappedBy = "district")
    @CascadeOnDelete
    private List<Subdistrict> subdistricts = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @ElementCollection
    @CollectionTable(name = "district_tag", joinColumns = @JoinColumn(name = "district"))
    @Column(name = "name", nullable = false)
    @CascadeOnDelete
    @OrderBy("name")
    private List<String> tags = new ArrayList<>();

    protected District() {
        super();
    }

    @JsonbCreator
    public District(String code) {
        super(code);
    }

    public District(String code, String name, double area) {
        super(code, name, area);
    }

    @Override
    public double discharge(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.getSubdistricts().stream()
                .mapToDouble(d -> d.specificDischarge(parameter, startDate, endDate) * d.getArea())
                .sum();
    }

    @Override
    public Stream<Station> findEnclosingStations(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.getSubdistricts().stream()
                .flatMap(sd -> sd.getRivers().stream())
                .filter(River::isInternalWater)
                .filter(r -> r.getParentRiver().filter(River::isInternalWater).isEmpty())
                .flatMap(r -> r.findEnclosingStations(parameter, startDate, endDate));
    }

    public Country getCountry() {
        return this.country;
    }

    @JsonbTransient
    public void setCountry(Country country) {

        country.getDistricts().add(this);

        if (this.getCountry() != null) {
            this.getCountry().getDistricts().remove(this);
        }

        this.country = country;

    }

    @Override
    public List<Estimate> getEstimates() {
        return this.estimates;
    }

    protected void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }

    public List<Subdistrict> getSubdistricts() {
        return this.subdistricts;
    }

    protected void setSubdistricts(List<Subdistrict> subdistricts) {
        this.subdistricts = subdistricts;
    }

    @Override
    public List<String> getTags() {
        return this.tags;
    }

    protected void setTags(List<String> tags) {
        this.tags = tags;
    }

}
