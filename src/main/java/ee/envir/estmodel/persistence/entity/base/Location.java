/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity.base;

import ee.envir.estmodel.model.Source;
import ee.envir.estmodel.persistence.entity.base.Estimate.Parameter;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import static java.lang.Double.NaN;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import org.eclipse.persistence.annotations.Cache;
import static org.eclipse.persistence.annotations.CacheType.SOFT;
import static org.eclipse.persistence.config.CacheIsolationType.PROTECTED;

@MappedSuperclass
@Cache(isolation = PROTECTED, type = SOFT)
@JsonbPropertyOrder({"code", "name"})
public abstract class Location implements Source {

    @Id
    @Column(name = "code", nullable = false, length = 15)
    private String code;

    @Basic(optional = false)
    @Column(name = "name", nullable = false)
    private String name;

    protected Location() {

    }

    protected Location(String code) {

        this.code = code;

    }

    protected Location(String code, String name) {

        this.code = code;
        this.name = name;

    }

    @Override
    public double discharge(Object parameter, LocalDate startDate, LocalDate endDate) {

        var values = this.getEstimates().stream()
                .filter(e -> e.getParameter() == parameter
                && !e.getStartDate().isBefore(startDate) && !e.getStartDate().isAfter(endDate))
                .mapToDouble(Estimate::getValue);

        return (parameter == Parameter.Q ? values.average() : values.reduce(Double::sum)).orElse(NaN);

    }

    @Override
    public final boolean equals(Object object) {
        return object instanceof Location other && Objects.equals(this.code, other.code);
    }

    @NotNull
    @Pattern(regexp = "^[A-Z0-9]+(_[A-Z0-9]+)*+$")
    @Size(min = 2, max = 15)
    public String getCode() {
        return this.code;
    }

    @NotBlank
    @Size(max = 255)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract List<Estimate> getEstimates();

    public abstract List<String> getTags();

    @Override
    public final int hashCode() {
        return Objects.hashCode(this.code);
    }

    @Override
    public final String toString() {
        return this.code;
    }

}
