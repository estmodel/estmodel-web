/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.model.RiverStation;
import ee.envir.estmodel.persistence.entity.base.Estimate;
import ee.envir.estmodel.persistence.entity.base.Estimate.Parameter;
import ee.envir.estmodel.persistence.entity.base.Subcatchment;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import static jakarta.persistence.EnumType.STRING;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.Noncacheable;

@Entity
@Table(name = "station")
@NamedQuery(name = "Station.findAll", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Station(s.code, s.name, s.area, s.overlapArea, s.countryArea, s.calculationArea, s.distance, s.type)
    FROM Station s LEFT JOIN s.tags t
    WHERE (:code IS NULL OR LOCATE(UPPER(:code), s.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(s.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY s HAVING :count <= COUNT(s)
    ORDER BY s.name, s.code
    """)
@NamedQuery(name = "Station.findByCountry", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Station(s.code, s.name, s.area, s.overlapArea, s.countryArea, s.calculationArea, s.distance, s.type)
    FROM Station s LEFT JOIN s.tags t JOIN s.river r JOIN r.district sd JOIN sd.district d
    WHERE :country = d.country.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), s.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(s.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY s HAVING :count <= COUNT(s)
    ORDER BY s.name, s.code
    """)
@NamedQuery(name = "Station.findByDistrict", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Station(s.code, s.name, s.area, s.overlapArea, s.countryArea, s.calculationArea, s.distance, s.type)
    FROM Station s LEFT JOIN s.tags t JOIN s.river r JOIN r.district sd
    WHERE :district = sd.district.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), s.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(s.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY s HAVING :count <= COUNT(s)
    ORDER BY s.name, s.code
    """)
@NamedQuery(name = "Station.findBySubdistrict", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Station(s.code, s.name, s.area, s.overlapArea, s.countryArea, s.calculationArea, s.distance, s.type)
    FROM Station s LEFT JOIN s.tags t JOIN s.river r
    WHERE :subdistrict = r.district.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), s.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(s.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY s HAVING :count <= COUNT(s)
    ORDER BY s.name, s.code
    """)
@NamedQuery(name = "Station.findByRiver", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Station(s.code, s.name, s.area, s.overlapArea, s.countryArea, s.calculationArea, s.distance, s.type)
    FROM Station s LEFT JOIN s.tags t
    WHERE :river = s.river.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), s.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(s.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY s HAVING :count <= COUNT(s)
    ORDER BY s.distance, s.area DESC, s.name, s.code
    """)
@JsonbPropertyOrder({"type", "river"})
public class Station extends Subcatchment<StationModel>
        implements RiverStation<Station> {

    @Enumerated(STRING)
    @Column(name = "type", length = 15)
    private Type type;

    @ManyToOne(optional = false)
    @JoinColumn(name = "river", nullable = false)
    private River river;

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true, targetEntity = StationEstimate.class)
    @JoinColumn(name = "station", nullable = false)
    @CascadeOnDelete
    private List<Estimate> estimates = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "station", nullable = false)
    @CascadeOnDelete
    @OrderBy("parameter, startDate")
    private List<StationMeasurement> measurements = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @ElementCollection
    @CollectionTable(name = "station_tag", joinColumns = @JoinColumn(name = "station"))
    @Column(name = "name", nullable = false)
    @CascadeOnDelete
    @OrderBy("name")
    private List<String> tags = new ArrayList<>();

    protected Station() {
        super();
    }

    @JsonbCreator
    public Station(String code) {
        super(code);
    }

    public Station(String code, String name, double area, double overlapArea,
            double countryArea, double calculationArea, double distance, Type type) {

        super(code, name, area, overlapArea, countryArea, calculationArea, distance);
        this.type = type;

    }

    @Override
    public Stream<Station> findEnclosingStations(Object parameter, LocalDate startDate, LocalDate endDate) {
        return this.getRiver()
                .findEnclosingStations(parameter, startDate, endDate, this.getDistance());
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public River getRiver() {
        return this.river;
    }

    @JsonbTransient
    public void setRiver(River river) {

        if (this.getRiver() != null) {
            this.getRiver().getStations().remove(this);
        }

        this.river = river;

        if (this.getRiver() != null) {
            this.getRiver().getStations().add(this);
        }

    }

    @Override
    public List<Estimate> getEstimates() {
        return this.estimates;
    }

    protected void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }

    public List<StationMeasurement> getMeasurements() {
        return this.measurements;
    }

    protected void setMeasurements(List<StationMeasurement> measurements) {
        this.measurements = measurements;
    }

    @Override
    public List<String> getTags() {
        return this.tags;
    }

    protected void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public boolean isMonitoring(Object parameter, LocalDate startDate, LocalDate endDate) {

        if (parameter == Parameter.Q || parameter == Parameter.V) {
            if (this.getType() == Type.HYDROCHEMICAL) {
                return false;
            }
        } else {
            if (this.getType() == Type.HYDROLOGICAL) {
                return false;
            }
        }

        return this.getEstimates().stream()
                .anyMatch(e -> e.getParameter() == parameter
                && !e.getStartDate().isBefore(startDate) && !e.getStartDate().isAfter(endDate));

    }

    public enum Type {

        HYDROCHEMICAL, HYDROLOGICAL;

    }

}
