/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity.base;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.validation.constraints.PositiveOrZero;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.Noncacheable;

@MappedSuperclass
public abstract class Catchment<M extends Model> extends Location {

    @Basic(optional = false)
    @Column(name = "area", nullable = false)
    private double area;

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "code", nullable = false)
    @CascadeOnDelete
    @OrderBy("year")
    private List<M> models = new ArrayList<>();

    protected Catchment() {
    }

    protected Catchment(String code) {
        super(code);
    }

    protected Catchment(String code, String name, double area) {
        super(code, name);
        this.area = area;
    }

    public List<M> getModels() {
        return this.models;
    }

    public void setModels(List<M> models) {
        this.models = models;
    }

    @PositiveOrZero
    public double getArea() {
        return this.area;
    }

    public void setArea(double area) {
        this.area = area;
    }

}
