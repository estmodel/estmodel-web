/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.persistence.entity.base.Catchment;
import ee.envir.estmodel.persistence.entity.base.Estimate;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.Noncacheable;

@Entity
@Table(name = "assessment_unit")
@NamedQuery(name = "AssessmentUnit.findAll", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.AssessmentUnit(au.code, au.name, au.area)
    FROM AssessmentUnit au LEFT JOIN au.tags t
    WHERE (:code IS NULL OR LOCATE(UPPER(:code), au.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(au.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY au HAVING :count <= COUNT(au)
    ORDER BY au.code
    """)
@NamedQuery(name = "AssessmentUnit.findByCountry", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.AssessmentUnit(au.code, au.name, au.area)
    FROM AssessmentUnit au LEFT JOIN au.tags t
    WHERE :country = au.country.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), au.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(au.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY au HAVING :count <= COUNT(au)
    ORDER BY au.code
    """)
public class AssessmentUnit extends Catchment<AssessmentUnitModel> {

    @ManyToOne(optional = false)
    @JoinColumn(name = "country", nullable = false)
    private Country country;

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true, targetEntity = AssessmentUnitEstimate.class)
    @JoinColumn(name = "assessment_unit", nullable = false)
    @CascadeOnDelete
    private List<Estimate> estimates = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @ElementCollection
    @CollectionTable(name = "assessment_unit_tag", joinColumns = @JoinColumn(name = "assessment_unit"))
    @Column(name = "name", nullable = false)
    @CascadeOnDelete
    @OrderBy("name")
    private List<String> tags = new ArrayList<>();

    protected AssessmentUnit() {
        super();
    }

    @JsonbCreator
    public AssessmentUnit(String code) {
        super(code);
    }

    public AssessmentUnit(String code, String name, double area) {
        super(code, name, area);
    }

    public Country getCountry() {
        return this.country;
    }

    @JsonbTransient
    public void setCountry(Country country) {

        if (this.getCountry() != null) {
            this.getCountry().getAssessmentUnits().remove(this);
        }

        this.country = country;

        if (this.getCountry() != null) {
            country.getAssessmentUnits().add(this);
        }

    }

    @Override
    public List<Estimate> getEstimates() {
        return this.estimates;
    }

    protected void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }

    @Override
    public List<String> getTags() {
        return this.tags;
    }

    protected void setTags(List<String> tags) {
        this.tags = tags;
    }

}
