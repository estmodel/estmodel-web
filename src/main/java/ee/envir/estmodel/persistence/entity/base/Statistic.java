/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity.base;

import jakarta.json.bind.annotation.JsonbPropertyOrder;
import java.io.Serializable;
import java.util.List;

@JsonbPropertyOrder({"maximum", "mean", "minimum", "monthlyStatistics"})
public class Statistic implements Serializable {

    private Measurement maximum;
    private Measurement mean;
    private Measurement minimum;
    private List<Statistic> monthlyStatistics;

    public Measurement getMaximum() {
        return this.maximum;
    }

    public void setMaximum(Measurement measurement) {
        this.maximum = measurement;
    }

    public Measurement getMean() {
        return this.mean;
    }

    public void setMean(Measurement measurement) {
        this.mean = measurement;
    }

    public Measurement getMinimum() {
        return this.minimum;
    }

    public void setMinimum(Measurement measurement) {
        this.minimum = measurement;
    }

    public List<Statistic> getMonthlyStatistics() {
        return this.monthlyStatistics;
    }

    public void setMonthlyStatistics(List<Statistic> statistics) {
        this.monthlyStatistics = statistics;
    }

}
