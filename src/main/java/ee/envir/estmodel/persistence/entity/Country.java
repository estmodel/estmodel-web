/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import ee.envir.estmodel.model.RiverBasinDistrict;
import ee.envir.estmodel.persistence.entity.base.Catchment;
import ee.envir.estmodel.persistence.entity.base.Estimate;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.annotations.Noncacheable;

@Entity
@Table(name = "country")
@NamedQuery(name = "Country.findAll", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Country(c.code, c.name, c.area)
    FROM Country c LEFT JOIN c.tags t
    WHERE (:code IS NULL OR LOCATE(UPPER(:code), c.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(c.name)) > 0)
    AND (:tags IS NULL OR t IN :tags)
    GROUP BY c HAVING :count <= COUNT(c)
    ORDER BY c.code
    """)
public class Country extends Catchment<CountryModel>
        implements RiverBasinDistrict<Station> {

    @JsonbTransient
    @Noncacheable
    @OneToMany(mappedBy = "country")
    @CascadeOnDelete
    private List<AssessmentUnit> assessmentUnits = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @OneToMany(orphanRemoval = true, targetEntity = CountryEstimate.class)
    @JoinColumn(name = "country", nullable = false)
    @CascadeOnDelete
    private List<Estimate> estimates = new ArrayList<>();

    @JsonbTransient
    @OneToMany(mappedBy = "country")
    @CascadeOnDelete
    private List<District> districts = new ArrayList<>();

    @JsonbTransient
    @Noncacheable
    @ElementCollection
    @CollectionTable(name = "country_tag", joinColumns = @JoinColumn(name = "country"))
    @Column(name = "name", nullable = false)
    @CascadeOnDelete
    @OrderBy("name")
    private List<String> tags = new ArrayList<>();

    protected Country() {
        super();
    }

    @JsonbCreator
    public Country(String code) {
        super(code);
    }

    public Country(String code, String name, double area) {
        super(code, name, area);
    }

    @Override
    public double discharge(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.getDistricts().stream()
                .mapToDouble(d -> d.discharge(parameter, startDate, endDate))
                .sum();
    }

    @Override
    public Stream<Station> findEnclosingStations(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.getDistricts().stream()
                .flatMap(d -> d.findEnclosingStations(parameter, startDate, endDate));
    }

    public List<AssessmentUnit> getAssessmentUnits() {
        return this.assessmentUnits;
    }

    protected void setAssessmentUnits(List<AssessmentUnit> assessmentUnits) {
        this.assessmentUnits = assessmentUnits;
    }

    @Override
    public List<Estimate> getEstimates() {
        return this.estimates;
    }

    protected void setEstimates(List<Estimate> estimates) {
        this.estimates = estimates;
    }

    public List<District> getDistricts() {
        return this.districts;
    }

    protected void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    @Override
    public List<String> getTags() {
        return this.tags;
    }

    protected void setTags(List<String> tags) {
        this.tags = tags;
    }

}
