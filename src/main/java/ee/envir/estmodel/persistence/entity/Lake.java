/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence.entity;

import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import static jakarta.persistence.FetchType.LAZY;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.PrimaryKey;
import static org.eclipse.persistence.config.CacheIsolationType.ISOLATED;

@Entity
@Table(name = "lake")
@NamedQuery(name = "Lake.deleteByRiver", query = """
    DELETE FROM Lake l
    WHERE :river = l.river.code
    """)
@NamedQuery(name = "Lake.findByRiver", query = """
    SELECT NEW ee.envir.estmodel.persistence.entity.Lake(l.code, l.name, l.distance, l.length)
    FROM Lake l
    WHERE :river = l.river.code
    AND (:code IS NULL OR LOCATE(UPPER(:code), l.code) > 0)
    AND (:name IS NULL OR LOCATE(UPPER(:name), UPPER(l.name)) > 0)
    ORDER BY l.distance
    """)
@PrimaryKey(columns = {
    @Column(name = "river"),
    @Column(name = "distance")
})
@Cache(isolation = ISOLATED)
@JsonbPropertyOrder({"code", "name", "distance", "length"})
public class Lake implements Serializable {

    @Column(name = "code", length = 15)
    private String code;

    @Column(name = "name", length = 255)
    private String name;

    @Id
    @Column(name = "distance")
    private double distance;

    @Basic(optional = false)
    @Column(name = "\"length\"", nullable = false)
    private double length;

    @JsonbTransient
    @ManyToOne(fetch = LAZY)
    @Id
    @JoinColumn(name = "river")
    private River river;

    public Lake() {
    }

    public Lake(String code, String name, double distance, double length) {
        this.code = code;
        this.name = name;
        this.distance = distance;
        this.length = length;
    }

    @Override
    public final boolean equals(Object object) {
        return object instanceof Lake other
                && this.distance == other.distance
                && Objects.equals(this.river, other.river);
    }

    @Pattern(regexp = "^[A-Z0-9]+(_[A-Z0-9]+)*+$")
    @Size(min = 2, max = 15)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Size(max = 255)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDistance() {
        return this.distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Positive
    public double getLength() {
        return this.length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public River getRiver() {
        return this.river;
    }

    public void setRiver(River river) {

        if (this.getRiver() != null) {
            this.getRiver().getLakes().remove(this);
        }

        this.river = river;

        if (this.getRiver() != null) {
            this.getRiver().getLakes().add(this);
        }

    }

    @Override
    public final int hashCode() {
        return Objects.hash(this.river, this.distance);
    }

    @Override
    public final String toString() {
        return this.code;
    }

}
