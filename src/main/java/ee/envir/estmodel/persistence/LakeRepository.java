/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.Lake;
import ee.envir.estmodel.persistence.entity.River;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceUnit;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class LakeRepository {

    @PersistenceUnit
    private EntityManagerFactory emf;

    public List<Lake> findByRiver(String riverCode,
            String code, String name, int offset, int limit) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var lakes = em.createNamedQuery("Lake.findByRiver", Lake.class)
                    .setParameter("river", riverCode)
                    .setParameter("code", code)
                    .setParameter("name", name)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();

            if (lakes.isEmpty() && em.find(River.class, riverCode) == null) {
                throw new EntityNotFoundException();
            }

            return lakes;

        }

    }

    protected EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Transactional
    public void replaceByRiver(String riverCode, Set<Lake> lakes) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.createNamedQuery("Lake.deleteByRiver")
                    .setParameter("river", riverCode)
                    .executeUpdate();

            var river = em.find(River.class, riverCode);

            if (river == null) {
                throw new EntityNotFoundException();
            }

            lakes.forEach(l -> l.setRiver(river));
            lakes.forEach(em::persist);

        }

    }

}
