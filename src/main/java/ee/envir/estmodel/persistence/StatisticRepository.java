/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.Station;
import ee.envir.estmodel.persistence.entity.base.Measurement;
import ee.envir.estmodel.persistence.entity.base.Measurement.Parameter;
import ee.envir.estmodel.persistence.entity.base.Statistic;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceUnit;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;
import static java.util.Collections.nCopies;
import java.util.LinkedHashMap;
import java.util.stream.Stream;

@ApplicationScoped
public class StatisticRepository {

    @PersistenceUnit
    private EntityManagerFactory emf;

    public Collection<Statistic> findByStation(String stationCode,
            Parameter parameter, int startYear, int endYear) {

        if (endYear == 0) {
            endYear = Year.now().getValue();
        }

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));
            em.setProperty("parameters", Stream.of(Parameter.values())
                    .filter(p -> parameter == p || parameter == null)
                    .map(Parameter::name).toList());

            var statistics = new LinkedHashMap<Parameter, Statistic>();

            for (var measurement : em.createNamedQuery("Measurement.findMeanByStation", Measurement.class)
                    .setParameter("station", stationCode)
                    .getResultList()) {

                var statistic = new Statistic();
                statistic.setMean(measurement);
                statistic.setMonthlyStatistics(new ArrayList<>(nCopies(12, null)));
                statistics.put(measurement.getParameter(), statistic);

            }

            for (var measurement : em.createNamedQuery("Measurement.findMaxByStation", Measurement.class)
                    .setParameter("station", stationCode)
                    .getResultList()) {

                statistics.get(measurement.getParameter()).setMaximum(measurement);

            }

            for (var measurement : em.createNamedQuery("Measurement.findMinByStation", Measurement.class)
                    .setParameter("station", stationCode)
                    .getResultList()) {

                statistics.get(measurement.getParameter()).setMinimum(measurement);

            }

            for (var measurement : em.createNamedQuery("Measurement.findMonthlyMeanByStation", Measurement.class)
                    .setParameter("station", stationCode)
                    .getResultList()) {

                var statistic = new Statistic();
                statistic.setMean(measurement);
                statistics.get(measurement.getParameter()).getMonthlyStatistics()
                        .set(measurement.getStartDate().getMonth().ordinal(), statistic);

            }

            for (var measurement : em.createNamedQuery("Measurement.findMonthlyMaxByStation", Measurement.class)
                    .setParameter("station", stationCode)
                    .getResultList()) {

                statistics.get(measurement.getParameter()).getMonthlyStatistics()
                        .get(measurement.getStartDate().getMonth().ordinal()).setMaximum(measurement);

            }

            for (var measurement : em.createNamedQuery("Measurement.findMonthlyMinByStation", Measurement.class)
                    .setParameter("station", stationCode)
                    .getResultList()) {

                statistics.get(measurement.getParameter()).getMonthlyStatistics()
                        .get(measurement.getStartDate().getMonth().ordinal()).setMinimum(measurement);

            }

            if (statistics.isEmpty() && em.find(Station.class, stationCode) == null) {
                throw new EntityNotFoundException();
            }

            return statistics.values();

        }

    }

    protected EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

}
