/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.Country;
import ee.envir.estmodel.persistence.entity.District;
import ee.envir.estmodel.persistence.entity.River;
import ee.envir.estmodel.persistence.entity.Subdistrict;
import ee.envir.estmodel.persistence.entity.WaterOutlet;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class WaterOutletRepository extends EntityRepository<WaterOutlet> {

    public List<WaterOutlet> findByCountry(String countryCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(Country.class, countryCode,
                limit, offset, code, name, tags);
    }

    public List<WaterOutlet> findByDistrict(String districtCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(District.class, districtCode,
                limit, offset, code, name, tags);
    }

    public List<WaterOutlet> findByRiver(String riverCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(River.class, riverCode,
                limit, offset, code, name, tags);
    }

    public List<WaterOutlet> findBySubdistrict(String subdistrictCode,
            int limit, int offset, String code, String name, String... tags) {

        return this.findBy(Subdistrict.class, subdistrictCode,
                limit, offset, code, name, tags);
    }

    @Override
    protected Class<WaterOutlet> getEntityClass() {
        return WaterOutlet.class;
    }

    @Transactional
    public void save(WaterOutlet outlet) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            var district = outlet.getDistrict();

            if (district != null) {
                district = em.find(Subdistrict.class, district.getCode());
            }

            var river = outlet.getRiver().orElse(null);

            if (river != null) {
                river = em.find(River.class, river.getCode());
            }

            if (district == null && river == null) {
                throw new EntityNotFoundException();
            }

            var entity = em.find(this.getEntityClass(), outlet.getCode());

            if (entity != null) {
                entity.setName(outlet.getName());
                entity.setDistance(outlet.getDistance());
                entity.setLength(outlet.getLength());
                entity.setType(outlet.getType());
                entity.setWaterType(outlet.getWaterType());
                if (river != null) {
                    entity.setRiver(river);
                } else {
                    entity.setRiver(null);
                    entity.setDistrict(district);
                }
            } else {
                if (river != null) {
                    outlet.setRiver(river);
                } else {
                    outlet.setRiver(null);
                    outlet.setDistrict(district);
                }
                em.persist(outlet);
            }

        }

    }

}
