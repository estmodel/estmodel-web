/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.Station;
import ee.envir.estmodel.persistence.entity.WaterOutlet;
import ee.envir.estmodel.persistence.entity.base.Estimate.TimeStep;
import ee.envir.estmodel.persistence.entity.base.Measurement;
import ee.envir.estmodel.persistence.entity.base.Measurement.Parameter;
import ee.envir.estmodel.persistence.entity.base.Measurement.Type;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceUnit;
import jakarta.transaction.Transactional;
import java.time.LocalDate;
import java.time.Year;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

@ApplicationScoped
public class MeasurementRepository {

    @PersistenceUnit
    private EntityManagerFactory emf;

    public List<Measurement> findByStation(String stationCode,
            Parameter parameter, Type type, int startYear, int endYear) {

        if (endYear == 0) {
            endYear = Year.now().getValue();
        }

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));
            em.setProperty("parameters", Stream.of(Parameter.values())
                    .filter(p -> parameter == p || parameter == null)
                    .map(Parameter::name).toList());

            var measurements = em.createNamedQuery("Measurement.findByStation", Measurement.class)
                    .setParameter("station", stationCode)
                    .setParameter("type", type)
                    .getResultList();

            if (measurements.isEmpty() && em.find(Station.class, stationCode) == null) {
                throw new EntityNotFoundException();
            }

            return measurements;

        }

    }

    public List<Measurement> findByWaterOutlet(String outletCode,
            Parameter parameter, Type type, int startYear, int endYear) {

        if (endYear == 0) {
            endYear = Year.now().getValue();
        }

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));
            em.setProperty("parameters", Stream.of(Parameter.values())
                    .filter(p -> parameter == p || parameter == null)
                    .map(Parameter::name).toList());

            var measurements = em.createNamedQuery("Measurement.findByWaterOutlet", Measurement.class)
                    .setParameter("wateroutlet", outletCode)
                    .setParameter("type", type)
                    .getResultList();

            if (measurements.isEmpty() && em.find(WaterOutlet.class, outletCode) == null) {
                throw new EntityNotFoundException();
            }

            return measurements;

        }

    }

    protected EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Transactional
    public void replaceByStation(String stationCode,
            Parameter parameter, Type type, int startYear, int endYear,
            TimeStep step, Set<Measurement> measurements) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));
            em.setProperty("parameters", List.of(parameter.name()));

            em.createNamedQuery("Measurement.deleteByStation")
                    .setParameter("station", stationCode)
                    .setParameter("type", type)
                    .executeUpdate();

            var station = em.find(Station.class, stationCode);

            if (station == null) {
                throw new EntityNotFoundException();
            }

            measurements.stream()
                    .filter(m -> m.getParameter() == parameter
                    && m.getStartDate().getYear() >= startYear
                    && m.getStartDate().getYear() <= endYear)
                    .forEach(m -> {
                        m.setType(type);
                        em.persist(m.with(station));
                    });

        }

    }

    @Transactional
    public void replaceByWaterOutlet(String outletCode,
            Parameter parameter, Type type, int startYear, int endYear,
            TimeStep step, Set<Measurement> measurements) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));
            em.setProperty("parameters", List.of(parameter.name()));

            em.createNamedQuery("Measurement.deleteByWaterOutlet")
                    .setParameter("wateroutlet", outletCode)
                    .setParameter("type", type)
                    .executeUpdate();

            var outlet = em.find(WaterOutlet.class, outletCode);

            if (outlet == null) {
                throw new EntityNotFoundException();
            }

            measurements.stream()
                    .filter(m -> m.getParameter() == parameter
                    && m.getStartDate().getYear() >= startYear
                    && m.getStartDate().getYear() <= endYear)
                    .forEach(m -> {
                        m.setType(type);
                        em.persist(m.with(outlet));
                    });

        }

    }

}
