/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel Server.
 *
 * EstModel Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with EstModel Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.persistence;

import ee.envir.estmodel.persistence.entity.AssessmentUnit;
import ee.envir.estmodel.persistence.entity.Country;
import ee.envir.estmodel.persistence.entity.CountryEstimate;
import ee.envir.estmodel.persistence.entity.District;
import ee.envir.estmodel.persistence.entity.DistrictEstimate;
import ee.envir.estmodel.persistence.entity.River;
import ee.envir.estmodel.persistence.entity.RiverEstimate;
import ee.envir.estmodel.persistence.entity.Station;
import ee.envir.estmodel.persistence.entity.StationEstimate;
import ee.envir.estmodel.persistence.entity.StationMeasurement;
import ee.envir.estmodel.persistence.entity.Subdistrict;
import ee.envir.estmodel.persistence.entity.SubdistrictEstimate;
import ee.envir.estmodel.persistence.entity.WaterOutlet;
import ee.envir.estmodel.persistence.entity.base.Estimate;
import ee.envir.estmodel.persistence.entity.base.Estimate.Parameter;
import ee.envir.estmodel.persistence.entity.base.Estimate.TimeStep;
import ee.envir.estmodel.persistence.entity.base.Location;
import ee.envir.estmodel.persistence.entity.base.Measurement;
import ee.envir.estmodel.persistence.entity.base.Measurement.QualityFlag;
import ee.envir.estmodel.persistence.entity.base.Measurement.Type;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceUnit;
import jakarta.transaction.Transactional;
import static java.lang.Double.isNaN;
import static java.lang.Double.max;
import java.time.LocalDate;
import java.time.Year;
import static java.util.Comparator.comparing;
import java.util.List;
import java.util.Set;
import static java.util.stream.IntStream.rangeClosed;

@ApplicationScoped
public class EstimateRepository {

    @PersistenceUnit
    private EntityManagerFactory emf;

    private <L extends Location> List<Estimate> findAll(
            Class<L> entityClass, String code, Parameter parameter,
            int startYear, int endYear, TimeStep step) {

        if (endYear == 0) {
            endYear = Year.now().getValue() - 1;
        }

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("parameters", List.of(parameter.name()));
            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));

            var estimates = em.createNamedQuery("Estimate.findBy"
                    + entityClass.getSimpleName()
                    + "GroupBy" + step, Estimate.class)
                    .setParameter(entityClass.getSimpleName().toLowerCase(), code)
                    .getResultList();

            if (estimates.isEmpty() && em.find(entityClass, code) == null) {
                throw new EntityNotFoundException();
            }

            return estimates;

        }

    }

    private List<Estimate> findAll(String riverCode,
            Parameter parameter, int startYear, int endYear, TimeStep step) {

        if (endYear == 0) {
            endYear = Year.now().getValue() - 1;
        }

        return rangeClosed(startYear, endYear).boxed().<Estimate>mapMulti((year, consumer) -> {

            var startDate = LocalDate.of(year, 1, 1);
            var endDate = LocalDate.of(year, 12, 31);

            try (var em = this.getEntityManagerFactory().createEntityManager()) {

                em.setProperty("parameters", List.of(parameter.name()));
                em.setProperty("startDate", startDate);
                em.setProperty("endDate", endDate);

                var river = em.getReference(River.class, riverCode);

                startDate.datesUntil(endDate.plusDays(1), step.toPeriod()).forEach(stepStartDate -> {

                    var stepEndDate = stepStartDate.plus(step.toPeriod()).minusDays(1);
                    double value = river.discharge(parameter, stepStartDate, stepEndDate);

                    if (!isNaN(value)) {
                        consumer.accept(new Estimate(parameter, stepStartDate, stepEndDate, value));
                    }

                });

            }

        }).toList();

    }

    private List<Estimate> findAll(String riverCode,
            Parameter parameter, int startYear, int endYear, TimeStep step,
            double distance, double calculationArea) {

        if (endYear == 0) {
            endYear = Year.now().getValue() - 1;
        }

        return rangeClosed(startYear, endYear).boxed().<Estimate>mapMulti((year, consumer) -> {

            var startDate = LocalDate.of(year, 1, 1);
            var endDate = LocalDate.of(year, 12, 31);

            try (var em = this.getEntityManagerFactory().createEntityManager()) {

                em.setProperty("parameters", List.of(parameter.name()));
                em.setProperty("startDate", startDate);
                em.setProperty("endDate", endDate);

                var river = em.getReference(River.class, riverCode);

                startDate.datesUntil(endDate.plusDays(1), step.toPeriod()).forEach(stepStartDate -> {

                    var stepEndDate = stepStartDate.plus(step.toPeriod()).minusDays(1);
                    double value = river.discharge(parameter, stepStartDate, stepEndDate, distance, calculationArea);

                    if (!isNaN(value)) {
                        consumer.accept(new Estimate(parameter, stepStartDate, stepEndDate, value));
                    }

                });

            }

        }).toList();

    }

    public List<Estimate> findByAssessmentUnit(String assessmentUnitCode,
            Parameter parameter, int startYear, int endYear, TimeStep step) {

        return this.findAll(AssessmentUnit.class, assessmentUnitCode,
                parameter, startYear, endYear, step);
    }

    public List<Estimate> findByCountry(String countryCode,
            Parameter parameter, int startYear, int endYear, TimeStep step) {

        return this.findAll(Country.class, countryCode,
                parameter, startYear, endYear, step);
    }

    public List<Estimate> findByDistrict(String districtCode,
            Parameter parameter, int startYear, int endYear, TimeStep step) {

        return this.findAll(District.class, districtCode,
                parameter, startYear, endYear, step);
    }

    public List<Estimate> findByRiver(String riverCode,
            Parameter parameter, int startYear, int endYear, TimeStep step,
            double distance, double area) {

        if (distance > 0) {

            try (var em = this.getEntityManagerFactory().createEntityManager()) {

                var river = em.getReference(River.class, riverCode);

                if (area > 0) {
                    area = river.calculationArea(distance, area);
                } else {
                    area = river.calculationArea(distance);
                }

            }

            return this.findAll(riverCode, parameter, startYear, endYear, step, distance, area);

        }

        return this.findAll(riverCode, parameter, startYear, endYear, step);

    }

    public List<Estimate> findByStation(String stationCode,
            Parameter parameter, int startYear, int endYear, TimeStep step) {

        return this.findAll(Station.class, stationCode,
                parameter, startYear, endYear, step);

    }

    public List<Estimate> findBySubdistrict(String subdistrictCode,
            Parameter parameter, int startYear, int endYear, TimeStep step) {

        return this.findAll(Subdistrict.class, subdistrictCode,
                parameter, startYear, endYear, step);
    }

    public List<Estimate> findByWaterOutlet(String outletCode,
            Parameter parameter, int startYear, int endYear, TimeStep step) {

        return this.findAll(WaterOutlet.class, outletCode,
                parameter, startYear, endYear, step);
    }

    private List<StationMeasurement> findCorrectedStationMeasurements(
            String stationCode, Parameter parameter,
            int startYear, int endYear) {

        return rangeClosed(startYear, endYear).boxed().flatMap(year -> {

            try (var em = this.getEntityManagerFactory().createEntityManager()) {

                em.setProperty("startDate", LocalDate.of(year, 1, 1));
                em.setProperty("endDate", LocalDate.of(year, 12, 31));
                em.setProperty("parameters", List.of(parameter.name()));

                var station = em.getReference(Station.class, stationCode);

                var measurements = station.getMeasurements().stream()
                        .filter(Type.MEAN
                                .and(QualityFlag.OUTLIER.negate())
                                .and(QualityFlag.UNCHECKED.negate())).toList();

                if (measurements.isEmpty()) {
                    return null;
                }

                var estimates = measurements.stream()
                        .filter(QualityFlag.BELOW_LOQ).toList();

                double estimateCount = estimates.size();
                double measurementCount = measurements.size();

                double coefficient = max(1 - estimateCount / measurementCount, 0.5);

                estimates.forEach(m -> {
                    em.detach(m);
                    m.setValue(m.getValue() * coefficient);
                });

                return measurements.stream();

            }

        }).toList();

    }

    protected EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    private <L extends Location, E extends Estimate> void replaceAll(
            Class<L> locationClass, String code, Parameter parameter,
            int startYear, int endYear, EstimateFunction<L, E> function) {

        rangeClosed(startYear, endYear).forEach(year -> {

            var startDate = LocalDate.of(year, 1, 1);
            var endDate = LocalDate.of(year, 12, 31);

            try (var em = this.getEntityManagerFactory().createEntityManager()) {

                em.setProperty("startDate", startDate);
                em.setProperty("endDate", endDate);
                em.setProperty("parameters", List.of(parameter.name()));

                em.createNamedQuery("Estimate.deleteBy" + locationClass.getSimpleName())
                        .setParameter(locationClass.getSimpleName().toLowerCase(), code)
                        .executeUpdate();

                var location = em.find(locationClass, code);

                startDate.datesUntil(endDate.plusDays(1)).forEach(date -> {

                    double value = location.discharge(parameter, date, date);

                    if (!isNaN(value)) {
                        em.persist(function.apply(location, parameter, date, value));
                    }

                });

            }

        });
    }

    @Transactional
    public void replaceByAssessmentUnit(String assessmentUnitCode,
            Parameter parameter, int startYear, int endYear,
            Set<Estimate> estimates) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));
            em.setProperty("parameters", List.of(parameter.name()));

            em.createNamedQuery("Estimate.deleteByAssessmentUnit")
                    .setParameter("assessmentunit", assessmentUnitCode)
                    .executeUpdate();

            var entity = em.getReference(AssessmentUnit.class, assessmentUnitCode);

            estimates.stream()
                    .filter(e -> e.getParameter() == parameter
                    && e.getStartDate().getYear() >= startYear
                    && e.getEndDate().getYear() <= endYear)
                    .map(m -> m.with(entity))
                    .forEach(em::persist);

        }

    }

    @Transactional
    public void replaceByCountry(String countryCode,
            Parameter parameter, int startYear, int endYear) {

        this.replaceAll(Country.class, countryCode, parameter, startYear, endYear,
                CountryEstimate::new);
    }

    @Transactional
    public void replaceByDistrict(String districtCode,
            Parameter parameter, int startYear, int endYear) {

        this.replaceAll(District.class, districtCode, parameter, startYear, endYear,
                DistrictEstimate::new);
    }

    @Transactional
    public void replaceByRiver(String riverCode,
            Parameter parameter, int startYear, int endYear) {

        this.replaceAll(River.class, riverCode, parameter, startYear, endYear,
                RiverEstimate::new);
    }

    @Transactional
    public void replaceByStation(String stationCode,
            Parameter parameter, int startYear, int endYear) {

        rangeClosed(startYear, endYear).forEach(year -> {

            var startDate = LocalDate.of(year, 1, 1);
            var endDate = LocalDate.of(year, 12, 31);

            try (var em = this.getEntityManagerFactory().createEntityManager()) {

                em.setProperty("startDate", startDate);
                em.setProperty("endDate", endDate);
                em.setProperty("parameters", List.of(parameter.name()));

                em.createNamedQuery("Estimate.deleteByStation")
                        .setParameter("station", stationCode)
                        .executeUpdate();

                em.setProperty("parameters", List.of(Parameter.Q.name()));

                var station = em.getReference(Station.class, stationCode);

                if (parameter == Parameter.V) {

                    startDate.datesUntil(endDate.plusDays(1)).forEach(date -> {

                        double value = station.discharge(Parameter.Q, date, date);

                        if (!isNaN(value)) {
                            em.persist(new StationEstimate(station, Parameter.V, date, value * 60 * 60 * 24));
                        }

                    });

                    return;

                }

                if (parameter == Parameter.Q && station.getType() == Station.Type.HYDROCHEMICAL) {

                    startDate.datesUntil(endDate.plusDays(1)).forEach(date -> {

                        double value = station.getRiver()
                                .discharge(Parameter.Q, date, date,
                                        station.getDistance(),
                                        station.getCalculationArea());

                        if (!isNaN(value)) {
                            em.persist(new StationEstimate(station, Parameter.Q, date, value));
                        }

                    });

                    return;

                }

                var measurements = this.findCorrectedStationMeasurements(stationCode,
                        parameter, year - 1, year + 1);

                if (measurements.stream().noneMatch(m -> m.getStartDate().getYear() == year)) {
                    return;
                }

                startDate.datesUntil(endDate.plusDays(1)).forEach(date -> {

                    var previous = measurements.stream()
                            .filter(m -> !m.getStartDate().isAfter(date))
                            .max(comparing(Measurement::getStartDate));

                    var next = measurements.stream()
                            .filter(m -> m.getStartDate().isAfter(date))
                            .min(comparing(Measurement::getStartDate));

                    previous.map(p -> next.map(n -> p.getValue()
                            + (p.getValue() - n.getValue())
                            * (date.toEpochDay() - p.getStartDate().toEpochDay())
                            / (p.getStartDate().toEpochDay() - n.getStartDate().toEpochDay()))
                            .orElse(p.getValue()))
                            .or(() -> next.map(Measurement::getValue))
                            .map(value -> {
                                var estimate = new StationEstimate();
                                estimate.setStation(station);
                                estimate.setParameter(parameter);
                                estimate.setStartDate(date);
                                estimate.setEndDate(date);
                                if (parameter == Parameter.Q) {
                                    estimate.setValue(value);
                                } else {
                                    estimate.setValue(station.discharge(Parameter.Q, date, date)
                                            * value * 60 * 60 * 24 / 1000000);
                                }
                                return estimate;
                            })
                            .filter(estimate -> !isNaN(estimate.getValue()))
                            .ifPresent(em::persist);

                });

            }

        });

    }

    @Transactional
    public void replaceBySubdistrict(String subdistrictCode, Parameter parameter, int startYear, int endYear) {

        this.replaceAll(Subdistrict.class, subdistrictCode, parameter, startYear, endYear,
                SubdistrictEstimate::new);
    }

    @Transactional
    public void replaceByWaterOutlet(String outletCode,
            Parameter parameter, int startYear, int endYear,
            Set<Estimate> estimates) {

        try (var em = this.getEntityManagerFactory().createEntityManager()) {

            em.setProperty("startDate", LocalDate.of(startYear, 1, 1));
            em.setProperty("endDate", LocalDate.of(endYear, 12, 31));
            em.setProperty("parameters", List.of(parameter.name()));

            em.createNamedQuery("Estimate.deleteByWaterOutlet")
                    .setParameter("wateroutlet", outletCode)
                    .executeUpdate();

            var entity = em.find(WaterOutlet.class, outletCode);

            if (entity == null) {
                throw new EntityNotFoundException();
            }

            estimates.stream()
                    .filter(e -> e.getParameter() == parameter
                    && e.getStartDate().getYear() >= startYear
                    && e.getEndDate().getYear() <= endYear)
                    .map(m -> m.with(entity))
                    .forEach(em::persist);

        }

    }

    @FunctionalInterface
    private interface EstimateFunction<L extends Location, E extends Estimate> {

        E apply(L source, Parameter parameter, LocalDate date, double value);

    }

}
